A Simple Morse Code Translator written in Swift
=======

Translate Morse Code dits and dats anywhere and share them on Facebook

* Translate Morse Code with correct syntax
* Brag to your friends on Facebook a morse code translation
* Works on iPhone, iPod, and iPad

Requirements
------
* XCode 6
* iOS 7 or higher SDK
* An iPhone, iPad, or iPod Touch running iOS 7.0 or newer

_A code signing key from Apple is required to deploy apps to a device.
Without a developer key, the app can only be installed on the iPhone/iPad Simulator._

How to Compile
------
1. Open _MorseTranslator.xcworkspace_ using your updated XCode 6.
2. Product -> Run

How to Use
------
1. Input Morse Code pattern on the top Text View
2. Enter Keyboard/Click on Translate Button
3. (Optional) Post on Facebook via Action Button on upper right hand navigation button

License
------
OpenSource