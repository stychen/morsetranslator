//
//  FacebookManager.swift
//  MorseTranslator
//
//  Created by Tan Mel Stychen on 2014/10/07.
//  Copyright (c) 2014年 Stychen. All rights reserved.
//

import Foundation


/******************************************************************************\
    #
    # FacebookManager
    #
\******************************************************************************/

class FacebookManager: NSObject {
    
    
    let kFacebookAppID = "372531582897247"
    let kMyFeedPath = "/me/feed"
    
    /*------------------------------------*\
        # Initialize: Singleton
    \*------------------------------------*/
    
    class var manager: FacebookManager {
        
    struct Singleton {
        static let manager = FacebookManager()
        }
        return Singleton.manager;
    }
    
    
    override init() {
        super.init()
        
        EasyFacebook.sharedInstance().facebookAppID = kFacebookAppID
        EasyFacebook.sharedInstance().facebookReadPermissions = ["email"]
        EasyFacebook.sharedInstance().facebookPublishPermissions = ["publish_actions"]
    }
    
    /*------------------------------------*\
        # Utilities
    \*------------------------------------*/
    
    func postMessage(#message: String, completion: ((error: NSError?)->())) {
        
        var parameters: Dictionary <String, String> = Dictionary()
        parameters["message"] = message
        
            EasyFacebook.sharedInstance().requestPublishPermissions({ (token) -> Void in
                
                FBRequestConnection.startForPostStatusUpdate(message, completionHandler: { (connection, result, error) -> Void in
                    completion(error: error)
                })
                
                return
                
                }, error: { (error) -> Void in
                    completion(error: error)
            })
    }
}
