//
//  Constants.swift
//  MorseTranslator
//
//  Created by Tan Mel Stychen on 2014/10/06.
//  Copyright (c) 2014年 Stychen. All rights reserved.
//

import UIKit

struct BorderConstants {
    static let weight: CGFloat = 0.5
    static let cornerRadius: CGFloat = 3.0
    static let blackColor: CGColorRef = UIColor.blackColor().CGColor
    static let grayColor: CGColorRef = UIColor.darkGrayColor().CGColor
}

struct MorseConstants {
    static let acceptableCharacters:NSCharacterSet = NSCharacterSet(charactersInString: "/-. ")
    static let spacePlaceholder : String = "空"
    
    static let characterToMorseDictionary:[String: String] = [
        "A" : ".-",
        "B" : "-...",
        "C" : "-.-.",
        "D" : "-..",
        "E" : ".",
        "F" : "..-.",
        "G" : "--.",
        "H" : "....",
        "I" : "..",
        "J" : ".---",
        "K" : "-.-",
        "L" : ".-..",
        "M" : "--",
        "N" : "-.",
        "O" : "---",
        "P" : ".--.",
        "Q" : "--.-",
        "R" : ".-.",
        "S" : "...",
        "T" : "-",
        "U" : "..-",
        "V" : "...-",
        "W" : ".--",
        "X" : "-..-",
        "Y" : "-.--",
        "Z" : "--..",
        "1" : ".----",
        "2" : "..---",
        "3" : "...--",
        "4" : "....-",
        "5" : ".....",
        "6" : "-....",
        "7" : "--...",
        "8" : "---..",
        "9" : "----.",
        "0" : "-----",
        " " : "/",
        "," : "--..--",
        "." : ".-.-.-",
        "?" : "..--..",
        "\'" : ".----.",
        "!" : "-.-.--",
        "/" : "-..-.",
        "(" : "-.--.",
        ")" : "-.--.-",
        "&" : ".-...",
        ":" : "---...",
        ";" : "-.-.-.",
        "=" : "-...-",
        "+" : ".-.-.",
        "-" : "-....-",
        "_" : "..--.-",
        "\"" : ".-..-.",
        "$" : "...-..-",
        "" : ".--.-."
    ]
    
    static let morseToCharacterDictionary:[String: String] = [
        ".-" : "A",
        "-..." : "B",
        "-.-." : "C",
        "-.." : "D",
        "." : "E",
        "..-." : "F",
        "--." : "G",
        "...." : "H",
        ".." : "I",
        ".---" : "J",
        "-.-" : "K",
        ".-.." : "L",
        "--" : "M",
        "-." : "N",
        "---" : "O",
        ".--." : "P",
        "--.-" : "Q",
        ".-." : "R",
        "..." : "S",
        "-" : "T",
        "..-" : "U",
        "...-" : "V",
        ".--" : "W",
        "-..-" : "X",
        "-.--" : "Y",
        "--.." : "0",
        ".----" : "1",
        "..---" : "2",
        "...--" : "3",
        "....-" : "4",
        "....." : "5",
        "-...." : "6",
        "--..." : "7",
        "---.." : "8",
        "----." : "9",
        "-----" : "0",
        "/" : MorseConstants.spacePlaceholder,
        "--..--" : ",",
        ".-.-.-" : ".",
        "..--.." : "?",
        ".----." : "\'",
        "-.-.--" : "!",
        "-..-." : "/",
        "-.--." : "(",
        "-.--.-" : ")",
        ".-..." : "&",
        "---..." : ":",
        "-.-.-." : ";",
        "-...-" : "=",
        ".-.-." : "+",
        "-....-" : "-",
        "..--.-" : "_",
        ".-..-." : "\"",
        "...-..-" : "$",
        ".--.-." : "",
    ]
}