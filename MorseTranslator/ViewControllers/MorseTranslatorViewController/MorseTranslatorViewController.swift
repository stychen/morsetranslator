//
//  MorseTranslatorViewController.swift
//  MorseTranslator
//
//  Created by Tan Mel Stychen on 2014/10/06.
//  Copyright (c) 2014年 Stychen. All rights reserved.
//

import UIKit


/******************************************************************************\
    #
    # MorseTranslatorViewController
    #
\******************************************************************************/

class MorseTranslatorViewController: UIViewController, UITextViewDelegate, UIActionSheetDelegate {
    
    
    /*------------------------------------*\
        # Constants
    \*------------------------------------*/
    
    let kTitle = "Morse Translator"
    let kInputTextViewPlaceholder = "Place your Morse Code here\nOnly accepts dots, dashes and spaces"
    let kOutputTextViewPlaceholder = "Waiting for morse code processing"
    let kUpdateTranslationButtonText = "Update Translation"
    let kCancel = "Cancel"
    let kShareFB = "Share in FB"
    
    @IBOutlet weak var inputTextView: SAMTextView!
    @IBOutlet weak var outputTextView: SAMTextView!
    
    @IBOutlet weak var updateTranslationButton: UIButton!
    
    var translationDictionary: [String:String]!
    
    /*------------------------------------*\
        # Initialize
    \*------------------------------------*/
    
    override func viewDidLoad() {
        
        automaticallyAdjustsScrollViewInsets = false;
        
        title = kTitle
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Action,
                                                                         target: self,
                                                                         action: "rightBarButtonItemTapped:")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "viewTapped:")
        view.addGestureRecognizer(tapGestureRecognizer)
        
        // setup subviews
        inputTextView.text = ""
        inputTextView.placeholder = kInputTextViewPlaceholder
        inputTextView.delegate = self
        inputTextView.editable = true
        inputTextView.layer.borderWidth = BorderConstants.weight
        inputTextView.layer.borderColor = BorderConstants.grayColor
        inputTextView.layer.cornerRadius = BorderConstants.cornerRadius
        
        outputTextView.text = ""
        outputTextView.backgroundColor = UIColor(white: 0.9, alpha: 1.0)
        outputTextView.placeholder = kOutputTextViewPlaceholder
        outputTextView.editable = false
        outputTextView.layer.borderWidth = BorderConstants.weight
        outputTextView.layer.borderColor = BorderConstants.grayColor
        outputTextView.layer.cornerRadius = BorderConstants.cornerRadius
        outputTextView.layer.masksToBounds = true
        
        updateTranslationButton.setTitle(kUpdateTranslationButtonText, forState: UIControlState.Normal)
    }
    
    
    /*------------------------------------*\
        # Utilities
    \*------------------------------------*/
    
    func translate(morseString: String) -> String {
        var processedString = morseString.stringByReplacingOccurrencesOfString("/", withString: " / ", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        // trim start and end string
        var stripedString: String = processedString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        // trim repeating spaces
        let regex: NSRegularExpression = NSRegularExpression.regularExpressionWithPattern("  +", options: NSRegularExpressionOptions.CaseInsensitive, error: nil)!
        stripedString = regex.stringByReplacingMatchesInString(stripedString, options: NSMatchingOptions.allZeros, range: NSMakeRange(0, countElements(stripedString)), withTemplate: " ")
        
        // parse morse tokens
        var morseTokens: [String] = stripedString.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        // translate morse tokens into output string
        var outputString: String = ""
        for morseCode in morseTokens {
            if (MorseConstants.morseToCharacterDictionary[morseCode] != nil) {
                outputString = outputString + MorseConstants.morseToCharacterDictionary[morseCode]!
            } else {
                outputString = outputString + "?"
            }
        }
        
        // final step replace space placeholder with a space
        outputString = outputString.stringByReplacingOccurrencesOfString(MorseConstants.spacePlaceholder, withString: " ", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        return outputString
    }
    
    func updateTranslation() {
        outputTextView.text = translate(inputTextView.text)
    }
    
    func showActions() {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: kCancel, destructiveButtonTitle: nil, otherButtonTitles: kShareFB)
        actionSheet.showInView(view)
    }
    
    
    /*------------------------------------*\
        # Actions
    \*------------------------------------*/
    
    func rightBarButtonItemTapped(sender: UIBarButtonItem) {
        showActions()
    }
    
    func viewTapped(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func updateTranslationButtonTapped(sender: AnyObject) {
        updateTranslation()
    }
    
    
    /*------------------------------------*\
        # Text View Delegate
    \*------------------------------------*/

    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        if (text == "\n") {
            textView.resignFirstResponder()
            updateTranslation()
            return false
        }
        
        if (text.rangeOfCharacterFromSet(MorseConstants.acceptableCharacters.invertedSet) != nil) {
            return false
        } else {
            return true
        }
    }
    
    
    /*------------------------------------*\
        # Action Sheet Delegate
    \*------------------------------------*/
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        let buttonTitle: String = actionSheet.buttonTitleAtIndex(buttonIndex)
        
        if buttonTitle == kShareFB {
            FacebookManager.manager.postMessage(message: "Morse: \(self.inputTextView.text) ; Translation: \(self.outputTextView.text)", completion: { (error) -> () in
                var alertView: UIAlertView = UIAlertView()
                alertView.addButtonWithTitle("OK")
                if error == nil {
                    alertView.title = "Posted"
                } else {
                    alertView.title = "Opps Something Went Wrong"
                }
                alertView.show()
            })
        } else {
            
        }
    }
}