//
//  AppDelegate.swift
//  MorseTranslator
//
//  Created by Tan Mel Stychen on 2014/10/06.
//  Copyright (c) 2014年 Stychen. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    
    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // initialize managers
        FacebookManager.manager
    
        return true
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        FBSession.activeSession().closeAndClearTokenInformation()
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String, annotation: AnyObject?) -> Bool {
        return FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication)
    }
}

